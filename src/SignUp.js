import { createUserWithEmailAndPassword } from "@firebase/auth"
import { useState } from "react"
import { useNavigate } from "react-router"
import { Link } from "react-router-dom"
import { auth } from "./firebase"


export const SignUp = () => {
  // Navigate 
  let navigate = useNavigate()

  // All States 
  const [Email, setEmail] = useState('')
  const [PassWord, setPassWord] = useState('');
  const [RepPass, setRepPass] = useState('');

  // onForm Submit
  const signupBtn = async (e) => {
    e.preventDefault() 

    try{ // If Both Password Matches 
      if (PassWord === RepPass) {
        const user = await createUserWithEmailAndPassword(
          auth, Email, PassWord
        ) 
        console.log(user)
        // Clear State after Submit
        setEmail('')
        setPassWord('')
        setRepPass('')
        navigate('/')
      } else alert("Your Password Doesn't Match!")
    } catch(err){
      console.log(err.message)
    }

  }
  return (
   <>
    <form onSubmit={signupBtn} className="  flex flex-col items-center">
    <h4 className='headStyle'> Signup </h4>

    
  
    <input className="InputField" id="Email"
    placeholder="Type Email"  value={Email}
    onChange={e=>setEmail(e.target.value)}  />
    
    <input className="InputField" 
    placeholder="Type Password" value={PassWord}
    onChange={e=>setPassWord(e.target.value)}  />

    <input className="InputField" value={RepPass}
    placeholder="Repeat Password" type="text"
    onChange={e=> setRepPass(e.target.value)}  />

    <button type="submit"
    className='btn'> SignUp </button>
    
    {/* === Footer === */}
    <div className=" mt-7" 
    > Already have an Account? 
    <Link className=" ml-2 font-bold"
    to="/login"
    > Login</Link> </div>
     
    </form>
    
   </>
  )
}