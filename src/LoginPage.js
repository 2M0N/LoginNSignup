import { Link } from "react-router-dom"

export const LoginPage = () => {
  return (
   <>
      <Link to="/login" className="ring-2 ring-gray-600 p-1
      rounded bg-gray-700 font-bold text-blue-100
       shadow-lg">
      Login </Link>
   </> 
  )
}