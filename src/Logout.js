import { signOut } from 'firebase/auth'
import { auth } from './firebase'

export const Logout = () => {

  const signout = async () => {
    signOut(auth)
  } 
  return (
   <>
    <span onClick={signout} className="ring-2 
    ring-gray-600 p-1
    rounded bg-gray-700 font-bold text-blue-100
    shadow-lg">
    Logout </span>
   </>
  )
}