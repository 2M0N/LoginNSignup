import { useState } from "react"
import { Link } from "react-router-dom"
import { LoginPage } from "./LoginPage"
import { Logout } from "./Logout"
import {onAuthStateChanged} from 'firebase/auth'
import { auth } from "./firebase"

export const Mainhead = () => {
  const [user,setuser] = useState()

  // Get Logged User Data
  onAuthStateChanged(auth,(currentUser)=>{
    setuser(currentUser)
    console.log(currentUser)
  }) 

  return (
   <div className="mh mb-10">
     <div> #nsbq</div>
     <h1 className="font-extrabold text-2xl
    text-gray-700 text-center shadow-md">
     <Link to="/"> MainHead.Com </Link></h1>

    <div className=" ml-10 text-right ">
    
      {user ? <Logout /> : <LoginPage />}
    </div>
   </div>
  )
}