import { signInWithEmailAndPassword } from "@firebase/auth";
import { useState } from "react"
import { Link } from "react-router-dom";
import { auth } from "./firebase";

export const Login = () => {
  // States
  const [Email, setEmail] = useState('')
  const [PassWord, setPassWord] = useState('');

  // onSubmit Form
  const submitBtn = async (e) => {
    e.preventDefault()

    try {
      const user = await signInWithEmailAndPassword(
        auth, Email,PassWord
      );
      console.log(user)
    }catch(error){
      alert("Something is Wrong, Please Recheck ")
    }
  }
  return (
  <>
    <form onSubmit={submitBtn} 
    className="flex flex-col items-center">
    <h4 className='headStyle'> Login </h4>

    
    <input className="InputField" 
    placeholder="Type Email" 
    
    onChange={(e)=> setEmail(e.target.value)}
    />
    {/* =========PassWord ========== */}
    <input className="InputField"
    placeholder="Type Password" 
    type="password"   
    onChange={(e)=> setPassWord(e.target.value)}
    />

    <button className=' btn'
    type="submit" > Submit </button>
    
    <div className="mt-6">

     <a className="  font-bold text-gray-800 
      cursor-pointer" href='2'> 
      Forget Password </a> or
    
    <Link  className="
    text-gray-800 font-bold cursor-pointer" 
    to="/signup">  SignUp!</Link>
    </div>
    
    </form>
    
  </>
  )
}