import * as React from "react";
import { Routes, Route } from "react-router-dom";
import { Home } from "./Home";
import { Login } from "./Login"
import { Mainhead } from "./Mainhead";
import { SignUp } from "./SignUp"
// import logo from  "./images/fb.svg"
// import './index.css'


const App = () => {
  return (
   <>
    <div className="main">
      <Mainhead/>
      <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/login" element={<Login />}  />
      <Route path="/signup" element={<SignUp />}  />
      </Routes>

      {/* <img alt='fb' src={logo}
      className="logo"  />

      <p className="pdec"> Facebook helps you connect and
      <br/> share 
        with the people in your life.
      </p>

      <form>
        <input type="text" 
        placeholder='Email address or phone number' />
        <input type="password" placeholder="Password" />

        <button> Log In </button>

        <p className='forgot'> Forgotten password?</p>
        
        <div className='cHolder'> 
        <button className='create'> Create New Account </button>
        </div>
      </form>
      <div className='footer'> <b> Create a Page </b>
        <span> for a celebrity, brand or business.</span> </div>*/}
      
    </div>
   </>
  )
}
export default App

